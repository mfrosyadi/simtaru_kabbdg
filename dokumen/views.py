import datetime
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.db.models import Q
from .models import Dokumen
from .forms import DokumenForm
# Create your views here.


@login_required(login_url='/kuisioner')
def daftar_dokumen(request):
    user = request.user
    context = {}
    doc = False
    if user.is_authenticated():
        # cari dokumen yang di upload oleh user dan yang berupa publik
        doc = Dokumen.objects.all().filter(Q(upload_by=user.id) | Q(berkas_publik=True))
    elif user.is_superuser:
        doc = Dokumen.objects.all()
    else:
        # untuk tamu
        context['tamu'] = True
    if doc:
        context['doc'] = doc
    return render(request, 'dokumen/dokumen.html', context)


@login_required(login_url='/kuisioner')
def upload_dokumen(request):
    if request.method == 'POST':
        form = DokumenForm(request.POST, request.FILES)
        if form.is_valid():
            d = Dokumen.objects.create(
                berkas=request.FILES['docfile'],
                upload_by=request.user,
                tgl_upload=datetime.datetime.now(),
                berkas_publik=form.cleaned_data.get('berkas_publik', False)
            )
            d.save()
            # Redirect to the document list after POST
            return HttpResponseRedirect('/dokumen')
    else:
        form = DokumenForm()  # A empty, unbound form

    # Load documents for the list page

    # Render list page with the documents and the form
    return render(request, 'dokumen/upload.html', {'form': form})


@login_required(login_url='/kuisioner')
def download_dokumen(request, dok_id):
    group = request.user.groups.all()[0]
    d = get_object_or_404(Dokumen, pk=dok_id, group=group)
    d.total_download += 1
    d.save()
    return HttpResponseRedirect(d.berkas.url)


def peta_rencana(request):
    return render(request, 'dokumen/peta_rencana.html')


def peta_dasar(request):
    return render(request, 'dokumen/peta_dasar.html')
