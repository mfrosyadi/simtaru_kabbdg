from django.contrib import admin
from .models import Dokumen


class DokumenAdmin(admin.ModelAdmin):
    # perbaikan di menu admin
    list_display = ('tgl_upload', 'berkas', 'upload_by', 'berkas_publik', 'total_download')

admin.site.register(Dokumen, DokumenAdmin)

