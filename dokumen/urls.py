from django.conf.urls import url

from dokumen import views

urlpatterns = [
    url(r'^$', views.daftar_dokumen, name='dokumen'),
    url(r'^download/(?P<dok_id>\d+)/$', views.download_dokumen, name='download'),
    url(r'^upload/$', views.upload_dokumen, name='upload'),
    url(r'^peta_dasar$', views.peta_dasar, name='peta_dasar'),
    url(r'^peta_rencana$', views.peta_rencana, name='peta_rencana'),
]
