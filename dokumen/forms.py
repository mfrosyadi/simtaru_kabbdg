from django import forms


class DokumenForm(forms.Form):
    docfile = forms.FileField(
        label='Pilih File'
    )
    berkas_publik = forms.BooleanField(label='Untuk Publik')
