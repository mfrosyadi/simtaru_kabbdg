from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Dokumen(models.Model):
    berkas = models.FileField(upload_to='dokumen')
    upload_by = models.ForeignKey(User)
    tgl_upload = models.DateTimeField(verbose_name="Tanggal Upload")
    total_download = models.IntegerField(verbose_name="Jumlah Download", default=0)
    berkas_publik = models.BooleanField(verbose_name="Untuk Publik", default=False)

    class Meta:
        ordering = ('-tgl_upload',)
