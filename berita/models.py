from django.db import models
from ckeditor.fields import RichTextField


class Berita(models.Model):
    judul = models.CharField(max_length=255)
    penulis = models.CharField(max_length=255)
    gambar = models.ImageField(upload_to='berita_foto')
    berita_pendek = models.CharField(max_length=255)
    berita_panjang = RichTextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-updated',)
        verbose_name_plural = 'Berita-berita'

    def __str__(self):
        return self.judul
