from django.contrib import admin
from .models import Berita


class BeritaAdmin(admin.ModelAdmin):
    # perbaikan di menu admin
    list_display = ('judul', 'penulis', 'updated')

admin.site.register(Berita)
