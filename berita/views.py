from django.shortcuts import render, get_object_or_404
from .models import Berita


def lihat_semua_berita(request):
    berita = Berita.objects.all()[:4]
    return render(request, 'berita/index.html', {'berita': berita})


def lihat_berita(request, berita_id):
    berita = get_object_or_404(Berita, pk=berita_id)
    return render(request, 'berita/detail_berita.html', {'berita': berita})
