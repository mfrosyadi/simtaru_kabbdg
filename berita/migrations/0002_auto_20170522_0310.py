# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-22 03:10
from __future__ import unicode_literals

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('berita', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='berita',
            name='berita_panjang',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
