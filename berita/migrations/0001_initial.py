# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-20 10:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Berita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('judul', models.CharField(max_length=255)),
                ('penulis', models.CharField(max_length=255)),
                ('gambar', models.ImageField(upload_to=b'berita_foto')),
                ('berita_pendek', models.CharField(max_length=255)),
                ('berita_panjang', models.TextField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ('-updated',),
            },
        ),
    ]
