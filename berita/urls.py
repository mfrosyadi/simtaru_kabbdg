from django.conf.urls import url

from berita import views

urlpatterns = [
    url(r'^$', views.lihat_semua_berita, name='semua_berita'),
    url(r'^detail/(?P<berita_id>\d+)/$', views.lihat_berita, name='sebuah_berita'),
]
