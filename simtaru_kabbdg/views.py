from django.shortcuts import render
from berita.models import Berita
from django.contrib.auth import logout, authenticate, login
from django.http import HttpResponseRedirect
from django.db.models import Count

from .forms import LoginForm
from kuisioner.models import Kuisioner
# Create your views here.


def home(request):
    berita = Berita.objects.all()[:4]
    return render(request, 'simtaru_kabbdg/home.html', {'berita': berita})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    return HttpResponseRedirect('/')
            else:
                return HttpResponseRedirect('/')
    else:
        form = LoginForm()
    return render(request, 'simtaru_kabbdg/login.html', {'form': form})


def profile_view(request):
    return render(request, 'simtaru_kabbdg/profile.html')


def kontak_view(request):
    # TODO: prosess kontak form
    if request.method == 'POST':
        return HttpResponseRedirect('/')
    return render(request, 'simtaru_kabbdg/kontak.html')


def grafik_view(request):
    # TODO: better permission handling
    if request.user.is_superuser:
        stat = Kuisioner.objects.values('alasan').annotate(total=Count('alasan')).order_by('alasan')
        return render(request, 'simtaru_kabbdg/grafik.html', {'stat': stat})
    else:
        return HttpResponseRedirect('/')
