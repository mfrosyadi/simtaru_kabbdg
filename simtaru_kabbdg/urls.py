from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from . import views

admin.autodiscover()

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^logout/', views.logout_view, name='logout'),
    url(r'^login/', views.login_view, name='login'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^profile/', views.profile_view, name='profile'),
    url(r'^grafik/', views.grafik_view, name='grafik'),
    url(r'^kontak/', views.kontak_view, name='kontak'),
    url(r'^peta/', include('lihat_peta.urls', namespace='peta')),
    url(r'^kuisioner/', include('kuisioner.urls', namespace='kuisioner')),
    url(r'^dokumen/', include('dokumen.urls', namespace='dokumen')),
    url(r'^berita/', include('berita.urls', namespace='berita')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
