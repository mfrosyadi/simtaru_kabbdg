"""
Django settings for simtaru_kabbdg project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'men#4qngsvuds_bsy2fsrm)-4$#b5@ze5frypyf=71-@a8e+!$'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'ckeditor',
    'ckeditor_uploader',
    'simtaru_kabbdg',
    'lihat_peta',
    'berita',
    'kuisioner',
    'dokumen',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'simtaru_kabbdg.urls'

WSGI_APPLICATION = 'simtaru_kabbdg.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'simtaru',
        'HOST': 'localhost',
        'USER': 'fauzan',
        'PASSWORD': 'fauzan'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = '/home/fauzan/Projects/python/django_static/'

EMAIL_HOST = 'smtp.mailtrap.io'
EMAIL_HOST_USER = 'c50fe83ce74952'
EMAIL_HOST_PASSWORD = 'b3a4a6f0bd4a15'
EMAIL_PORT = 25

# django ck-editor settings
# When using default file system storage, images will be uploaded to
# CKEDITOR_UPLOAD_PATH folder in your MEDIA_ROOT and urls will be created
# against MEDIA_URL (example: /media/CKEDITOR_UPLOAD_PATH/image.jpg).
CKEDITOR_UPLOAD_PATH = 'media_berita/'
CKEDITOR_JQUERY_URL = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'

# Geoserver web url, do not put trailing '/'
GEOURL = 'http://128.199.100.103/geoserver'
# settingan koneksi ke postgis
PGDB = 'djangis'
PGUSR = 'fauzan'
PGPWD = 'fauzan'
PGHOST = '128.199.100.103'
PGPORT = '5432'