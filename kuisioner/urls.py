from django.conf.urls import url

from kuisioner import views

urlpatterns = [
    url(r'^$', views.tampilkan_kuisioner, name='kuisioner'),
    url(r'^token', views.tamu_login),
    url(r'detail/(?P<alasan>\w+)/', views.detail_statistik)
]
