from django import forms


PILIHAN = (
    ('pendidikan', 'Pendidikan'),
    ('pekerjaan', 'Pekerjaan'),
    ('perijinan', 'Perijinan'),
    ('pemerintahan', 'Pemerintahan'),
)


class KuisionerForm(forms.Form):
    """
    memasukan name email dan alasan,
    password akan dibuat automatis sebagai token yang dikirim ke email
    untuk usernamenya memakai session ID
    """
    alasan = forms.ChoiceField(choices=PILIHAN, widget=forms.RadioSelect)
    nama = forms.CharField(max_length=255)
    tlp = forms.CharField(max_length=255)
    email = forms.EmailField()
