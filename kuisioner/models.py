from django.db import models

PILIHAN = (
    ('pendidikan', 'Pendidikan'),
    ('pekerjaan', 'Pekerjaan'),
    ('perijinan', 'Perijinan'),
    ('pemerintahan', 'Pemerintahan'),
)


class Kuisioner(models.Model):
    alasan = models.CharField(
        max_length=50, choices=PILIHAN, default='pendidikan')
    nama = models.CharField(max_length=255)
    email = models.EmailField()
    tlp = models.CharField(max_length=255, default='1111')
