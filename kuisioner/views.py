import random
import json

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.core.mail import send_mail


from .models import Kuisioner
from .forms import KuisionerForm


def gen_passwd():
    charset = 'ABCDEF1234567890'
    return ''.join(random.choice(charset) for _ in range(7))


def tampilkan_kuisioner(request):
    """
    Hanya menampilkan kuisioner, view ini juga berfungsi untuk melakukan
    registrasi user. Setelah itu halaman untuk mengisi password ditampilkan.
    user baru dibuat dengan session_id sebagi username
    """
    if request.method == 'POST':
        form = KuisionerForm(request.POST)
        if form.is_valid():
            k = Kuisioner(
                nama=form.cleaned_data['nama'],
                alasan=form.cleaned_data['alasan'],
                email=form.cleaned_data['email'],
                tlp=form.cleaned_data['tlp'],
            )
            k.save()
            password = gen_passwd()
            username = request.session._get_new_session_key()[:10]
            u = User.objects.create_user(
                username, password=password, first_name=form.cleaned_data['nama'])
            u.save()
            request.session['tamu'] = username
            send_mail(
                'Token akses Peta',
                'Untuk mengakses peta silahkan gunakan kode ini: %s' % password,
                'admin@simtaru.kabbdg.go.id',
                [form.cleaned_data['email']]
            )
            return render(request, 'kuisioner/masukan_token.html')
    else:
        form = KuisionerForm()

    return render(request, 'kuisioner/kuisioner.html', {
        'form': form,
    })


def tamu_login(request):
    """
    prosess login untuk tamu setelah itu di redirect ke halaman peta
    """
    username = request.session['tamu']
    password = request.POST.get('token', '')
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        del request.session['tamu']
        request.session.set_expiry(300)
        print "SESSION WILL EXPIRE IN 300 SECONDS"
        return HttpResponseRedirect('/')
    else:
        print "GA LOGIN"
        # TODO: Tampilkan kalo token salah, generate ulang token lalu kirim lagi
        return HttpResponseRedirect('/')
    pass


def detail_statistik(request, alasan):
    ret = []
    k_all = Kuisioner.objects.all().filter(alasan=alasan.lower())

    return render(request, 'kuisioner/detail_stat.html', {'detail': k_all, 'alasan': alasan})
