// Pastikan file ini terletak setelah <div id="map" class="map"></div>
// Layer untuk peta dasar
// Secure Ajax
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

var kabupatenLayer = new ol.layer.Image({
    title: 'Kabupaten',
    source:  new ol.source.ImageWMS({
            url: geourl+'/wms',
            params: {
                'LAYERS': 'SIMTARU:kabupaten',
                'FORMAT': 'image/png'
            },
            serverType: 'geoserver',
            crossOrigin: '*'
    })
});

var kabupatenBatasLayer = new ol.layer.Image({
    title: 'Batas Kabupaten',
    source:  new ol.source.ImageWMS({
            url: geourl+'/wms',
            params: {
                'LAYERS': 'SIMTARU:kabupaten_batas',
                'FORMAT': 'image/png'
            },
            serverType: 'geoserver',
            crossOrigin: '*'
    })
});

var kecamatanLayer = new ol.layer.Image({
    title: 'Kecamatan',
    source:  new ol.source.ImageWMS({
            url: geourl+'/wms',
            params: {
                'LAYERS': 'SIMTARU:kecamatan',
                'FORMAT': 'image/png'
            },
            serverType: 'geoserver',
            crossOrigin: '*'
    }),
    visible: true
});

var kecamatanBatasLayer = new ol.layer.Image({
    title: 'Batas Kecamatan',
    source:  new ol.source.ImageWMS({
            url: geourl+'/wms',
            params: {
                'LAYERS': 'SIMTARU:kecamatan_batas',
                'FORMAT': 'image/png'
            },
            serverType: 'geoserver',
            crossOrigin: '*'
    }),
    visible: false
});

var desaLayer = new ol.layer.Image({
    title: 'Desa',
    source: new ol.source.ImageWMS({
        url: geourl+'/wms',
        params: {
            'LAYERS': 'SIMTARU:desa',
            'FORMAT': 'image/png'
        },
        serverType: 'geoserver',
        crossOrigin: '*'
    }),
    visible: true
});

var desaBatasLayer = new ol.layer.Image({
    title: 'Batas Desa',
    source: new ol.source.ImageWMS({
        url: geourl+'/wms',
        params: {
            'LAYERS': 'SIMTARU:desa_batas',
            'FORMAT': 'image/png'
        },
        serverType: 'geoserver',
        crossOrigin: '*'
    })
});

var jalanLayer = new ol.layer.Image({
    title: 'Jalan',
    source: new ol.source.ImageWMS({
        url: geourl+'/wms',
        params: {
            'LAYERS': 'SIMTARU:jalan',
            'FORMAT': 'image/png'
        },
        serverType: 'geoserver',
        crossOrigin: '*'
    }),
    visible: false
});

var provinsiLayer = new ol.layer.Image({
    title: 'Provinsi',
    source: new ol.source.ImageWMS({
        url: geourl+'/wms',
        params: {
            'LAYERS': 'SIMTARU:provinsi',
            'FORMAT': 'image/png'
        },
        serverType: 'geoserver',
        crossOrigin: '*'
    })
});


// Deklarasi layer group untuk peta dasar

var petaDasar = new ol.layer.Group({
    title: 'Peta Administratif',
    openInLayerSwitcher: false,
    visible: true,
    layers: [kecamatanLayer, desaLayer,  kabupatenLayer]
});

 /**
  * Elements that make up the popup.
  */
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');


/**
 * Create an overlay to anchor the popup to the map.
 */
var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
element: container,
autoPan: true,
autoPanAnimation: {
    duration: 250
}
}));


/**
 * Add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
};

var mousePositionControl = new ol.control.MousePosition({
    className: 'ol-custom-mouseposition',
    coordinateFormat: ol.coordinate.createStringXY(4),
    projection: 'EPSG:4326',
    undefinedHTML: '&nbsp;'
});

var raster = new ol.layer.Tile({
    source: new ol.source.OSM()
  });
var map = new ol.Map({
    layers: [
        new ol.layer.Group({
            'title': 'Petanya',
            displayInLayerSwitcher: false,
            layers: [
                raster,
            ]
        }),
        petaDasar,
        vectorLayer,
    ],
    controls: [
        mousePositionControl,
        new ol.control.ZoomSlider(),
        new ol.control.ScaleLine(),
    ],
    overlays: [overlay],
    target: 'map',
    view: new ol.View({
      center: ol.proj.transform([107.5853139, -6.9840138], 'EPSG:4326', 'EPSG:3857'),
      zoom: 11,
      minZoom: 9,
      maxZoom: 17
    })
});

/*
 * Add a click handler to the map to render the popup.
 */
map.on('singleclick', function(evt) {
    // proyeksi coordinate 3857 untuk internal map
    var coordinate = evt.coordinate;
    map.getView().setCenter(coordinate);
    overlay.setPosition(coordinate);
    var feature = map.forEachFeatureAtPixel(evt.pixel,
        function(feature) {
            return feature;
        }
    );
    if (feature) {
        content.innerHTML = '<p><strong>Nama Kecamatan: </strong>'+feature.get('kecamatan')+'</p><p><strong>Nama Kelurahan: </strong>'+feature.get('kelurahan')+'</p><p><strong>Lokasi: </strong>'+feature.get('lokasi')+'</p><p><strong>Operator: </strong>'+feature.get('operator')+'<br/><br><a class="btn btn-danger" href="/peta/menara/delete/'+feature.getId()+'/">Hapus Data</a>';
    } else {
        content.innerHTML = '<code>loading...</code>';
        // untuk interface pake proyeksi 4326
        var newpos = ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326')
        $.ajax({
            url: 'ajax/cek_lokasi_menara/',
            data: {"long": coordinate[0], "lat": coordinate[1]},
            type: 'post',
            dataType: 'json',
            error: function (request, status, error) {
                        console.log(request.responseText);
                    },
            success: function(r){
                if (Object.getOwnPropertyNames(r).length === 0){
                    content.innerHTML = '<p>Tidak ada data kecamatan/desa di koordinat ini</p><code>' + newpos +'</code>';
                } else{
                    content.innerHTML = '<p><strong>'+r['kecamatan']+'</strong></p><code>' + newpos +'</code><br/> <a class="btn btn-primary" data-toggle="modal" data-target="#myModal" data-tooltip href="/peta/menara/update/?kecamatan='+r['kecamatan']+'&longitude='+newpos[0]+'&latitude='+newpos[1]+'">Tambah Data</a>';
                };
            }
        });
    }
});

$('a.sikaku').on('click', function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    $(".modal-body").html('<iframe width="100%" height="100%" frameborder="0" scrolling="no" allowtransparency="true" src="'+url+'"></iframe>');
});