// Pastikan file ini terletak setelah <div id="map" class="map"></div>
// Layer untuk peta dasar
// Secure Ajax
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

var rdtrLayer = new ol.layer.Image({
    title: 'RDTR',
    source:  new ol.source.ImageWMS({
            url: geourl+'/wms',
            params: {
                'LAYERS': 'SIMTARU:92rdtr_pola_ruang_merg',
                'FORMAT': 'image/png'
            },
            serverType: 'geoserver',
            crossOrigin: '*'
    })
});


var kecamatanLayer = new ol.layer.Image({
    title: 'Kecamatan',
    source:  new ol.source.ImageWMS({
            url: geourl+'/wms',
            params: {
                'LAYERS': 'SIMTARU:kecamatan',
                'FORMAT': 'image/png'
            },
            serverType: 'geoserver',
            crossOrigin: '*'
    }),
    visible: true
});



var petaDasar = new ol.layer.Group({
    title: 'Peta RDTR',
    openInLayerSwitcher: false,
    visible: true,
    layers: [rdtrLayer, kecamatanLayer]
});

/*
lg = http://127.0.0.1:8080/geoserver/rest/workspaces/SIMTARU/layergroups/peta_rancang_pola.json
group_title = lg['layerGroup']['title']
list_layer = lg['layerGroup']['publishables']['published']
*/
 /**
  * Elements that make up the popup.
  */
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');


/**
 * Create an overlay to anchor the popup to the map.
 */
var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
element: container,
autoPan: true,
autoPanAnimation: {
    duration: 250
}
}));


/**
 * Add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
};

var mousePositionControl = new ol.control.MousePosition({
    className: 'ol-custom-mouseposition',
    coordinateFormat: ol.coordinate.createStringXY(4),
    projection: 'EPSG:4326',
    undefinedHTML: '&nbsp;'
});

var overviewMapControl = new ol.control.OverviewMap({
    className: 'ol-overviewmap ol-custom-overviewmap',
    collapseLabel: '\u00BB',
    label: '\u00AB',
    collapsed: false,
    layers: [
        new ol.layer.Tile({
            title: 'Stamen - Water color',
            type: 'base',
            source: new ol.source.Stamen({
                layer: 'terrain'
            })
        }),
    ]
});
var raster = new ol.layer.Tile({
        source: new ol.source.OSM()
      });
var map = new ol.Map({
    layers: [
       new ol.layer.Group({
           'title': 'Petanya',
           displayInLayerSwitcher: false,
           layers: [
               raster,
           ]
       }),
        petaDasar,
    ],
    controls: [
        mousePositionControl,
        new ol.control.ZoomSlider(),
        new ol.control.ScaleLine(),
        overviewMapControl
    ],
    overlays: [overlay],
    target: 'map',
    view: new ol.View({
      center: ol.proj.transform([107.5853139, -6.9840138], 'EPSG:4326', 'EPSG:3857'),
      zoom: 13,
      minZoom: 9,
      maxZoom: 17
    })
});
 /**
 * Add a click handler to the map to render the popup.
 */
map.on('singleclick', function(evt) {
    // proyeksi coordinate 3857 untuk internal map
    var coordinate = evt.coordinate;
    map.getView().setCenter(coordinate);
    overlay.setPosition(coordinate);
    content.innerHTML = '<code>loading...</code>';
    // untuk interface pake proyeksi 4326
    var newpos = ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326') 
    $.ajax({
        url: 'ajax/get_peruntukan_koordinat/',
        data: {"long": coordinate[0], "lat": coordinate[1]},
        type: 'post',
        dataType: 'json',
        // error: function (request, status, error) {
        //             console.log(request.responseText);
        //         },
        success: function(r){
            if (Object.getOwnPropertyNames(r).length === 0){
                content.innerHTML = '<p>Tidak ada data peruntukan di koordinat ini</p><code>' + newpos +'</code>';

            } else{
                content.innerHTML = '<p><strong>'+r['kecamatan']+'</strong></p><p><strong>'+'Desa '+r['desa']+'</strong></p><p>Peruntukan: '+r['pola']+'</p><p>Luas: '+r['luas']+'</p><code>' + newpos +'</code>';

            };
        }
    });
});
// Add a layer switcher outside the map
var switcher = new ol.control.LayerSwitcher({
    target:$(".layerSwitcher").get(0),
    show_progress:true,
    extent: true,
    trash: false,
    reordering: false
});

map.addControl(switcher);
// Insert mapbox layer in layer switcher
function displayInLayerSwitcher(b)
{
    mapbox.set('displayInLayerSwitcher', b);
}
// TODO: split files
$.ajax({
    url: 'ajax/get_kecamatan/',
    type: 'post',
    dataType: 'json',
    success: function(result)
    {
        // return array of json
        $( "#cari_kecamatan" ).autocomplete({
            source: result['kecamatan']
        });
    },
    error: function (request, status, error) {
                        console.log(request.responseText);
                    }
});


$('#tipe_pencarian').change(function() {
    var val = $(this).val();
    $('#nama').addClass('hidden');
    $('#koordinat').addClass('hidden');
    $('#' + val).removeClass('hidden');
}).change();

$('#btn_cari_koordinat').click(function(e){
    var long =$('#cari_longitude').val();
    var lat = $('#cari_latitude').val();
    if (long.length == 0 | lat.length == 0){
        alert('Harap mengisi koordinat');
    }
    else {
        var newpos = ol.proj.transform([ parseFloat(long),  parseFloat(lat)], 'EPSG:4326', 'EPSG:3857');
        map.getView().setCenter(newpos);
        overlay.setPosition(newpos);
        map.getView().setZoom(17);
        content.innerHTML = '<code>loading...</code>';
        $.ajax({
            url: 'ajax/get_peruntukan_koordinat/',
            data: {"long": newpos[0], "lat":newpos[1]},
            type: 'post',
            dataType: 'json',
            error: function (request, status, error) {
                        console.log(request);
                        console.log(status);
                        console.log(error);
                    },
            success: function(r){
                if (Object.getOwnPropertyNames(r).length === 0){
                    content.innerHTML = '<p>Tidak ada data peruntukan di koordinat ini</p><code>' + newpos +'</code>';

                } else{
                    content.innerHTML = '<p><strong>'+r['kecamatan']+'</strong></p><p><strong>'+'Desa '+r['desa']+'</strong></p><p>Peruntukan: '+r['pola']+'</p><p>Luas: '+r['luas']+'</p><code>' + newpos +'</code>';

                };
            }
        });
    }
    e.preventDefault();
});


function setLokasi(long, lat){
    map.getView().setCenter([ parseFloat(long),  parseFloat(lat)]);
    map.getView().setZoom(17);
}

$('#btn_cari_nama').click(function(e){
    var desa = $('#cari_desa').val();
    var kecamatan = $('#cari_kecamatan').val();
    if (kecamatan.length == 0){
        alert('Harap mengisi nama Kecamatan');
    }
    else {
        var koordinat = map.getView().getCenter();
        $.ajax({
            url: 'ajax/get_peruntukan/',
            data: {"kecamatan": kecamatan,},
            type: 'post',
            dataType: 'json',
            error: function (request, status, error) {
                        console.log(request.responseText);
                    },
            success: function(r){
                // Reset active tabs
                $('.tabbable li').removeClass('active');
                $('#tabcontent').addClass('active')
                $('#tablist div').removeClass('active');
                // point to desired tab by making it active
                if (typeof r.length == "undefined"){
                    $('#tab3').empty().addClass('active').append('<strong>Tidak ada data</strong>');
                } else {
                    $('#tab3').empty().addClass('active').append('<strong>Data Peruntukan untuk '+kecamatan+'</strong>');
                    // Pengisian data
                    $('#tab3').append('<table class="table table-condensed"><thead><tr><td>Peruntukan</td><td>Luas(m<sup>2</sup>)</td><td>Lokasi</td></tr></thead><tbody></tbody></table>');
                    for (var i = 0; i < r.length; i++){
                        $('#tab3 table tbody').append('<tr><td>'+r[i]['pola']+'</td><td>'+r[i]['luas']+'</td><td><a class="btn btn-primary" onclick="setLokasi('+r[i]['lokasi'][0]+','+r[i]['lokasi'][1]+')">Lihat</a></td></tr>');
                    }
                    // Update lokasi peta
                    var long = r[0]['lokasi'][0];
                    var lat = r[0]['lokasi'][1];
                    koordinat = [parseFloat(long),  parseFloat(lat)];
                    map.getView().setCenter(koordinat);
                    map.getView().setZoom(15);

                }
            }
        });
    }
    e.preventDefault();
});
