# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-04 19:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lihat_peta', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek1',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek10',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek11',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek12',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek13',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek14',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek15',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek16',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek17',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek18',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek19',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek2',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek3',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek4',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek5',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek6',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek7',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek8',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek9',
            field=models.PositiveIntegerField(choices=[(1, 1), (3, 3), (5, 5)]),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='aspek_total',
            field=models.PositiveIntegerField(blank=True, default=19, verbose_name=b'Total'),
        ),
        migrations.AlterField(
            model_name='daerahkumuh',
            name='kategori',
            field=models.CharField(blank=True, default=b'TIDAK KUMUH', max_length=50),
        ),
    ]
