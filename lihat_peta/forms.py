from django.forms import ModelForm, ChoiceField
from .models import DaerahKumuh, MenaraBts


class DaerahKumuhForm(ModelForm):
    class Meta:
        model = DaerahKumuh
        fields = '__all__'


class MenaraBtsForm(ModelForm):
    class Meta:
        model = MenaraBts
        fields = '__all__'