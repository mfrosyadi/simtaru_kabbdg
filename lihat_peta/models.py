from django.db import models
from django.core.validators import RegexValidator

# Create your models here.


class DaerahKumuh(models.Model):
    ASPEK_VALUE = (
        (1, 1),
        (3, 3),
        (5, 5)
    )

    kecamatan = models.CharField(max_length=128)
    desa = models.CharField(max_length=128)
    lokasi = models.CharField(max_length=128)
    luas_area = models.FloatField(verbose_name='Luas Area')
    longitude = models.FloatField()
    latitude = models.FloatField()
    foto_kondisi = models.ImageField(upload_to='peta_daerahkumuh', blank=True)
    nama_surveyor = models.CharField(max_length=255)
    tlp_surveyor = models.CharField(
        max_length=255,
        validators=[RegexValidator(
            regex='[0-9\+\-]+', message='Hanya bisa angka dan tanda + dan -')])
    kategori = models.CharField(max_length=50, default='TIDAK KUMUH', blank=True)
    # TODO: enaknya dijadiin model terpisah
    aspek1 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek2 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek3 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek4 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek5 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek6 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek7 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek8 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek9 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek10 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek11 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek12 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek13 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek14 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek15 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek16 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek17 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek18 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek19 = models.PositiveIntegerField(choices=ASPEK_VALUE)
    aspek_total = models.PositiveIntegerField(
        verbose_name='Total', default=19, blank=True)

    def save(self, *args, **kwargs):
        self.aspek_total = self.aspek1 + self.aspek2 + self.aspek3 + self.aspek4 + self.aspek5 + self.aspek6 + self.aspek7 + self.aspek8 + self.aspek9 + self.aspek10 + self.aspek11 + self.aspek12 + self.aspek13 + self.aspek14 + self.aspek15 + self.aspek16 + self.aspek17 + self.aspek18 + self.aspek19
        if self.aspek_total < 19:
            self.kategori = 'TIDAK KUMUH'
        elif 44 >= self.aspek_total >= 19:
            self.kategori = 'KUMUH RINGAN'
        elif 70 >= self.aspek_total >= 45:
            self.kategori = 'KUMUH SEDANG'
        else:
            self.kategori = 'KUMUH BERAT'
        super(DaerahKumuh, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s di %s/%s - %s' % (self.lokasi, self.kecamatan, self.desa, self.kategori)


class MenaraBts(models.Model):
    longitude = models.FloatField()
    latitude = models.FloatField()
    kecamatan = models.CharField(max_length=128)
    kelurahan = models.CharField(max_length=128, blank=True)
    lokasi = models.CharField(max_length=128, blank=True)
    operator = models.CharField(max_length=256, blank=True)

    def __unicode__(self):
        return '%s di %s/%s - %s' % (self.lokasi, self.kecamatan, self.kelurahan, self.operator)
