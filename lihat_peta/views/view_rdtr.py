import json
import psycopg2

from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect


def index(request):
    return render(request, 'lihat_peta/index_rdtr.html',
                      {'GEOURL': settings.GEOURL})


def get_kecamatan(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    cursor.execute('SELECT DISTINCT nm_kec FROM "92rdtr_pola_ruang_merg"')
    res = cursor.fetchall()
    conn.close()
    data = {'kecamatan': [str(r[0]) for r in res]}
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')


def get_desa(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    kecamatan = str(request.POST.get('kecamatan', False))
    cursor = conn.cursor()
    cursor.execute('SELECT DISTINCT desa FROM data_peruntukan WHERE kecamatan=%s', [kecamatan])
    res = cursor.fetchall()
    conn.close()
    data = {'desa': [str(r[0]) for r in res]}
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')


def get_peruntukan(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    kecamatan = str(request.POST.get('kecamatan', False))
    desa = str(request.POST.get('desa', False))
    query = """
SELECT ST_AsText(ST_Transform(ST_SetSRID(ST_Centroid(geom), 32748), 3857)) as posisi, pola_ruang, luas
FROM "92rdtr_pola_ruang_merg" WHERE nm_kec=%s
"""
    cursor.execute(query, [kecamatan])
    res = cursor.fetchall()
    conn.close()
    ret = []
    for r in res:
        data = {}
        data['pola'] = r[1]
        data['luas'] = float(r[2])
        data['lokasi'] = str(r[0]).lstrip('POINT(').rstrip(')').split(' ')
        ret.append(data)
    dump = json.dumps(ret)
    return HttpResponse(dump, content_type='application/json')


def get_peruntukan_koordinat(request):
    """

    Dari file .shp proyeksinya menggunakan 32748 sementara dari openlayers
    proyeksinya 3857 jadi di transformasikan dulu dari 32748 ke 3857

    """
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    lat = float(request.POST.get('lat', False))
    long = float(request.POST.get('long', False))
    query = """
SELECT r.pola_ruang,r.luas,r.nm_kec
FROM data_peruntukan d, "92rdtr_pola_ruang_merg" r
WHERE ST_Contains(
    ST_Transform(ST_SetSRID(r.geom, 32748), 3857),
    ST_GeometryFromText('POINT(%s %s)', 3857)
) 
"""

    cursor.execute(query, [long, lat])
    res = cursor.fetchall()
    query2 = """
SELECT d.kecamatan,d.desa
FROM data_peruntukan d
WHERE ST_Contains(
    ST_Transform(ST_SetSRID(d.geom, 32748), 3857),
    ST_GeometryFromText('POINT(%s %s)', 3857)
)
"""
    cursor.execute(query2, [long, lat])
    res2 = cursor.fetchall()
    conn.close()
    data = {}
    if len(res) > 0 and len(res2) > 0:
        data = {
            'pola': res[0][0],
            'luas': float(res[0][1]),
            'kecamatan': res[0][2],
            'desa': res2[0][1]
        }
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')