import json
import psycopg2

from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect


def index(request):
    if request.user.is_authenticated():
        return render(request, 'lihat_peta/index.html',
                      {'GEOURL': settings.GEOURL})
    else:
        return HttpResponseRedirect('/kuisioner')


def get_kecamatan(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    cursor.execute('SELECT DISTINCT kecamatan FROM data_peruntukan')
    res = cursor.fetchall()
    conn.close()
    data = {'kecamatan': [str(r[0]) for r in res]}
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')


def get_desa(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    kecamatan = str(request.POST.get('kecamatan', False))
    cursor = conn.cursor()
    cursor.execute('SELECT DISTINCT desa FROM data_peruntukan WHERE kecamatan=%s', [kecamatan])
    res = cursor.fetchall()
    conn.close()
    data = {'desa': [str(r[0]) for r in res]}
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')


def get_peruntukan(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    kecamatan = str(request.POST.get('kecamatan', False))
    desa = str(request.POST.get('desa', False))
    query = """
SELECT ST_AsText(ST_Transform(ST_SetSRID(ST_Centroid(geom), 32748), 3857)) as posisi,pola,luas
FROM data_peruntukan WHERE kecamatan=%s AND desa=%s
"""
    cursor.execute(query, [kecamatan, desa])
    res = cursor.fetchall()
    conn.close()
    ret = []
    for r in res:
        data = {}
        data['pola'] = r[1]
        data['luas'] = float(r[2])
        data['lokasi'] = str(r[0]).lstrip('POINT(').rstrip(')').split(' ')
        ret.append(data)
    dump = json.dumps(ret)
    return HttpResponse(dump, content_type='application/json')


def get_peruntukan_koordinat(request):
    """

    Dari file .shp proyeksinya menggunakan 32748 sementara dari openlayers
    proyeksinya 3857 jadi di transformasikan dulu dari 32748 ke 3857

    """
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    lat = float(request.POST.get('lat', False))
    long = float(request.POST.get('long', False))
    query = """
SELECT kecamatan,desa,pola,luas
FROM data_peruntukan
WHERE ST_Contains(
    ST_Transform(ST_SetSRID(geom, 32748), 3857),
    ST_GeometryFromText('POINT(%s %s)', 3857)
)"""

    cursor.execute(query, [long, lat])
    res = cursor.fetchall()
    conn.close()
    data = {}
    if len(res) > 0:
        data = {
            'pola': res[0][2],
            'luas': float(res[0][3]),
            'kecamatan': res[0][0],
            'desa': res[0][1]
        }
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')