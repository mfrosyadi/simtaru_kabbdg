import psycopg2
import json

from django.conf import settings
from django.shortcuts import render, HttpResponse, get_object_or_404, HttpResponseRedirect
from django.views.decorators.csrf import ensure_csrf_cookie

from lihat_peta.models import MenaraBts
from lihat_peta.forms import MenaraBtsForm

@ensure_csrf_cookie
def cek_lokasi_menara(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    lat = float(request.POST.get('lat', False))
    long = float(request.POST.get('long', False))
    query = """
    SELECT kecamatan
    FROM data_peruntukan
    WHERE ST_Contains(
        ST_Transform(ST_SetSRID(geom, 32748), 3857),
        ST_GeometryFromText('POINT(%s %s)', 3857)
    )"""

    cursor.execute(query, [long, lat])
    res = cursor.fetchall()
    conn.close()
    data = {}
    if len(res) > 0:
        data = {
            'kecamatan': res[0][0],
        }
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')

@ensure_csrf_cookie
def get_lokasi_menara(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    query = """
    SELECT gid,longdesima,latdesimal,kecamatan,kelurahan,lokasi,luas_site,operator
    FROM "91menara_bts_all_font_point"
    """

    cursor.execute(query)
    res = cursor.fetchall()
    conn.close()
    lokasi_db = MenaraBts.objects.all()
    data = {'lokasi': [], 'lokasi_db': lokasi_db}
    table = ['id', 'longitude', 'latitude', 'kecamatan', 'kelurahan',
             'lokasi', 'luas_site', 'operator']
    if len(res) > 0:
        for r in res:
            data['lokasi'].append(dict(zip(table, r)))
    return render(request, 'lihat_peta/menara_bts.html', data)

@ensure_csrf_cookie
def set_lokasi_menara(request, pk=None):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    if request.method == 'POST':
        form = MenaraBtsForm(request.POST, request.FILES)
        if form.is_valid():
            if not pk:
                form.save()
            else:
                MenaraBts.objects.filter(pk=int(pk)).update(**form.cleaned_data)
                lokasi = get_object_or_404(MenaraBts, pk=pk)
                lokasi.save()
        return HttpResponseRedirect('/peta/menara/')
    elif pk:
        lokasi = get_object_or_404(MenaraBts, pk=pk)
        form = MenaraBtsForm(instance=lokasi)
        return render(request, 'lihat_peta/menara_bts_update.html', {'form': form})
    else:
        required = ('latitude', 'longitude', 'kecamatan')
        if all(map(lambda d: d in required, request.GET.keys())):
            initial_data = {
                'longitude': float(request.GET.get('longitude', 0.0)),
                'latitude': float(request.GET.get('latitude', 0.0)),
                'kecamatan': request.GET.get('kecamatan', '---'),
            }
            form = MenaraBtsForm(initial=initial_data)

            return render(request, 'lihat_peta/menara_bts_update.html', {'form': form})
        else:
            return HttpResponseRedirect('/peta/menara/')

@ensure_csrf_cookie
def delete_lokasi_menara(request, pk):
    lokasi = get_object_or_404(MenaraBts, pk=pk)
    lokasi.delete()
    return HttpResponseRedirect('/peta/menara/')