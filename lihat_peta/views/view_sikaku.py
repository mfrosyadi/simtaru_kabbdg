import json
import psycopg2

from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import ensure_csrf_cookie

from lihat_peta.forms import DaerahKumuhForm
from lihat_peta.models import DaerahKumuh


@ensure_csrf_cookie
def set_lokasi_kumuh(request):
    conn = psycopg2.connect(
        user=settings.PGUSR, password=settings.PGPWD,
        database=settings.PGDB, host=settings.PGHOST, port=settings.PGPORT
    )
    cursor = conn.cursor()
    lat = float(request.POST.get('lat', False))
    long = float(request.POST.get('long', False))
    query = """
    SELECT kecamatan,desa
    FROM data_peruntukan
    WHERE ST_Contains(
        ST_Transform(ST_SetSRID(geom, 32748), 3857),
        ST_GeometryFromText('POINT(%s %s)', 3857)
    )"""

    cursor.execute(query, [long, lat])
    res = cursor.fetchall()
    conn.close()
    data = {}
    if len(res) > 0:
        data = {
            'kecamatan': res[0][0],
            'desa': res[0][1]
        }
    dump = json.dumps(data)
    return HttpResponse(dump, content_type='application/json')


@ensure_csrf_cookie
def peta_daerahkumuh(request):
    lokasi = DaerahKumuh.objects.all()
    data = {}
    if lokasi:
        data['lokasi'] = lokasi
    return render(request, 'lihat_peta/daerah_kumuh.html', data)


@ensure_csrf_cookie
def get_lokasi_kumuh(request, pk=None):
    if request.method == 'POST':
        form = DaerahKumuhForm(request.POST, request.FILES)
        if form.is_valid():
            if not pk:
                form.save()
            else:
                DaerahKumuh.objects.filter(pk=int(pk)).update(**form.cleaned_data)
                lokasi = get_object_or_404(DaerahKumuh, pk=pk)
                lokasi.save()
        return HttpResponseRedirect('/peta/sikaku/')
    elif pk:
        lokasi = get_object_or_404(DaerahKumuh, pk=pk)
        form = DaerahKumuhForm(instance=lokasi)
        return render(request, 'lihat_peta/daerah_kumuh_update.html', {'form': form})
    else:
        required = ('latitude', 'longitude', 'kecamatan', 'desa')
        if all(map(lambda d: d in required, request.GET.keys())):
            initial_data = {
                'longitude': float(request.GET.get('longitude', 0.0)),
                'latitude': float(request.GET.get('latitude', 0.0)),
                'kecamatan': request.GET.get('kecamatan', '---'),
                'desa': request.GET.get('desa', '---')
            }
            form = DaerahKumuhForm(initial=initial_data)

            return render(request, 'lihat_peta/daerah_kumuh_update.html', {'form': form})
        else:
            return HttpResponseRedirect('/peta/sikaku/')


@ensure_csrf_cookie
def delete_lokasi_kumuh(request, pk):
    lokasi = get_object_or_404(DaerahKumuh, pk=pk)
    lokasi.delete()
    return HttpResponseRedirect('/peta/sikaku/')