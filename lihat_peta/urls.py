from django.conf.urls import url

from lihat_peta.views import view_simtaru, view_sikaku, view_menara, view_rdtr

urlpatterns = [
    url(r'^$', view_simtaru.index, name='index'),
    url(r'^ajax/get_kecamatan/$', view_simtaru.get_kecamatan),
    url(r'^ajax/get_desa/$', view_simtaru.get_desa),
    url(r'^ajax/get_peruntukan/$', view_simtaru.get_peruntukan),
    url(r'^ajax/get_peruntukan_koordinat/$', view_simtaru.get_peruntukan_koordinat),
    # SIKAKU, manajemen daerah kumuh
    url(r'^sikaku/$', view_sikaku.peta_daerahkumuh),
    url(r'^sikaku/update/$', view_sikaku.get_lokasi_kumuh, name='create_sikaku'),
    url(r'^sikaku/update/(?P<pk>\d+)/$', view_sikaku.get_lokasi_kumuh, name='update_sikaku'),
    url(r'^sikaku/delete/(?P<pk>\d+)/$', view_sikaku.delete_lokasi_kumuh, name='delete_sikaku'),
    url(r'^sikaku/ajax/set_lokasi_kumuh/$', view_sikaku.set_lokasi_kumuh),
    # Menara
    url(r'^menara/$', view_menara.get_lokasi_menara),
    url(r'^menara/update/$', view_menara.set_lokasi_menara, name='create_menara'),
    url(r'^menara/update/(?P<pk>\d+)/$', view_menara.set_lokasi_menara, name='update_sikaku'),
    url(r'^menara/delete/(?P<pk>\d+)/$', view_menara.delete_lokasi_menara, name='delete_sikaku'),
    url(r'^menara/ajax/cek_lokasi_menara/$', view_menara.cek_lokasi_menara),
    # RDTR
    url(r'^rdtr/$', view_rdtr.index),
    url(r'^rdtr/ajax/get_peruntukan_koordinat/$', view_rdtr.get_peruntukan_koordinat),
    url(r'^rdtr/ajax/get_kecamatan/$', view_rdtr.get_kecamatan),
    url(r'^rdtr/ajax/get_peruntukan/$', view_rdtr.get_peruntukan),
]
